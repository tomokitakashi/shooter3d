using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    GameObject timerText; //タイマー表示
    GameObject pointText; //スコア表示
    float time = 30.0f;　//初期タイム
    int point = 0; //初期スコア

    public void HitEnemy() //弾が敵に当たったらスコア加算
    {
        this.point += 100;
    }

    // Start is called before the first frame update
    void Start()
    {
        this.timerText = GameObject.Find("Time");
        this.pointText = GameObject.Find("Point");
    }

    // Update is called once per frame
    void Update()
    {
        this.time -= Time.deltaTime; //タイマー減少

        this.timerText.GetComponent<Text>().text =
           this.time.ToString("F1");　//１フレーム単位でタイム更新

        if(this.time == 0.0f)
        {
            SceneManager.LoadScene("retry");
        }

        this.pointText.GetComponent<Text>().text =
            this.point.ToString() + " point"; //スコア加算
    }
}
