﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameObject EnemyPrehub;

    public float minTime = 1f;
    public float maxTime = 4f;

    private float minX = -15f;
    private float maxX = 15f;
    private float minY = 18.5f;
    private float maxY = 27f;

    private float interval;

    private float time = 0f;

    public float speed = -0.03f;

    public void SetParameter(float minTime,float maxTime, float speed)
    {
        this.minTime = minTime;
        this.maxTime = maxTime;
        this.speed = speed;
    }

    // Start is called before the first frame update
    void Start()
    {
        interval = GetRandomTime();
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;

        if (time > interval)
        {
            GameObject enemy = Instantiate(EnemyPrehub);

            enemy.transform.position = GetRandomPosition();
            enemy.transform.Translate(0f, 0f, this.speed);
            time = 0f;

        }
        
    }

    private Vector3 GetRandomPosition()
    {
        float x = Random.Range(minX, maxX);
        float y = Random.Range(minY, maxY);

        return new Vector3(x, y, 20);
    }
    

    void sidemove()
    {
       transform.Translate(-0.1f, 0f, 0f); 
       transform.Translate(0.1f, 0f, 0f);
    }

    private float GetRandomTime()
    {
        return Random.Range(this.minTime, this.maxTime);
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("hit!");
        Destroy(gameObject);
        Destroy(other.gameObject);
    }
}
