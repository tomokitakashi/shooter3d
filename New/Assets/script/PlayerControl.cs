﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    public GameObject Bullet;
    public GameObject muzzle;
    
    public float speed = 3.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(0f, 0f, -0.1f);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(0f, 0f, 0.1f);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(-0.15f, 0f, 0f);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(0.15f, 0f, 0f);
        }
       
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0f, 0.1f, 0f);
        }
       
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(0f, -0.1f, 0f);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject CreateBullet = Instantiate(Bullet) as GameObject;
            CreateBullet.transform.position = muzzle.transform.position;
        }

        transform.position = new Vector3(
        Mathf.Clamp(transform.position.x, +-15f, 15f),
        Mathf.Clamp(transform.position.y, +18.5f, 27.0f),
        Mathf.Clamp(transform.position.z, +-6.0f, 2.0f)
        );

    }
}
