﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotBullet : MonoBehaviour
{
    GameObject director;

    // Start is called before the first frame update
    void Start()
    {
        this.director = GameObject.Find("GameDirector");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0f, 0f, 1f);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ememy")　//敵に当たった時の処理
        {
            Debug.Log("Tag=Ememy");
            this.director.GetComponent<GameDirector>().HitEnemy(); //Directorのスコア加算処理を呼び出す
            Destroy(gameObject);
        }
    }
}
