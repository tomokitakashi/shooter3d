﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    public GameObject Play;

    // Start is called before the first frame update
    void Start()
    {
        GameObject player = Instantiate(Play);

        player.transform.position = new Vector3(0, 20, -7);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
